# Indoor Solar Power Tracing Platform

## General Information

The repo presents a platform for long-term tracing solar harvesting that is designed as an I²C extension for the [RocketLogger platform](https://rocketlogger.ethz.ch). In connection with a RocketLogger, it allows measuring the energy flow at the input and the output of a bq25505 harvesting circuit and logging of the ambient sensors.

The platform consists of a solar panel (AM-5412) connected to a bq25505 energy harvesting chip that stores the harvested energy in a virtual battery circuit. The battery emulation circuit keeps the harvester output operating point fixed at at with configurable voltage level to guarantee consistent and comparable harvesting measurements. Two TSL45315 light sensors placed on opposite sides of the solar panel monitor the illuminance level and a BME280 sensor logs additional ambient conditions like temperature, humidity and air pressure.


## Long-Term Monitoring Campaign

The presented platform is sucessfully used in a long-term monitoring deployment at our institute (at ETH Zurich, Zurich, Switzerland) to collect diverse indoor harvesting traces. The following workshop publication gives an overview of this effort:

> L. Sigrist, A. Gomez, and L. Thiele.
> *Dataset: Tracing Indoor Solar Harvesting*.
> In Proceedings of the 2nd Workshop on Data Acquisition To Analysis (DATA '19), 2019.

The 2 years of data collected at the time of the release is available as dataset with further documentation and example code for post-processing:

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3363925.svg)](https://doi.org/10.5281/zenodo.3363925)


## Project Authors

* Author: Lukas Sigrist
* Contact: Lukas Sigrist <lukas.sigrist@tik.ee.ethz.ch>


## Changelog

* v1.0 (2017-06-07): Final version deployed at TIK next to some FlockLab locations


## Copyright and License

* Copyright: (c) 2017-2019, ETH Zurich, Computer Engineering Group.
* License: Creative Commons Attribution 4.0 International License (<http://creativecommons.org/licenses/by/4.0/>)
